from PyLIFA import *
import time
import matplotlib.pyplot as plt

device = PyLIFA('COM4')

if device.sync():
    print("Connected")
else:
    print("Error")
    exit()


# servo and double analog read
X = []
Y1 = []
Y2 = []

fig = plt.figure()
ax1 = fig.add_subplot(111)
plt.ion()
fig.show()
fig.canvas.draw()
servo = 6

device.setNumberOfServos(1)
device.configureServo(0,servo)

for x in range(0,180,10):
    device.servoWriteAngle(0,x)
    time.sleep(0.5)
    y1 = device.analogReadPin(0)
    y2 = device.analogReadPin(1)
    X.append(x)
    Y1.append(y1)
    Y2.append(y2)
    ax1.clear()
    ax1.plot(X, Y1, '-ob')
    ax1.plot(X, Y2, '-or')
    fig.canvas.draw()

plt.ioff()
plt.show()

# motor with L293DN
# dir_p = 4
# dir_n = 2
# pwm = 3
#
# sensor = 0
#
# device.setDigitalPinMode(dir_n, PyLIFA.pinMode["Output"])
# device.setDigitalPinMode(dir_p, PyLIFA.pinMode["Output"])
# device.setDigitalPinMode(pwm, PyLIFA.pinMode["Output"])
# device.digitalWritePin(dir_n, 0)
# device.digitalWritePin(dir_p, 1)
#
# for x in range(5):
#     device.PWMWritePin(pwm, 255)
#     time.sleep(1)
#     device.PWMWritePin(pwm, 128)
#     time.sleep(1)
#     device.PWMWritePin(pwm, 0)
#     time.sleep(1)


# i2c and analog input demo
# device.I2CInit()
# X = []
# Y = []
#
# fig = plt.figure()
# ax1 = fig.add_subplot(111)
# plt.ion()
# fig.show()
# fig.canvas.draw()
#
# for i in range(0, 4095, 10):
#     device.I2CWrite(0b1100001, [int(i / 256), int(i % 256)])
#     i2 = 4095 - i
#     device.I2CWrite(0b1100010, [int(i2 / 256), int(i2 % 256)])
#     voltage = device.analogReadPin(0)
#     X.append(i)
#     Y.append(voltage)
#     ax1.clear()
#     ax1.plot(X, Y, '-ob')
#     fig.canvas.draw()
#
# plt.ioff()
# plt.show()


# finite samples
# samples = device.getFiniteAnalogSample(0,1000,1000)
# plt.plot(samples)
# plt.show()

# continuous (fast) acquisition
# device.continousAcquisitionOn(0,1000)
# samples = device.continousAcquisitionSample(1000,1000)
# device.continousAcquisitionOff()
# plt.plot(samples)
# plt.show()

# single analog readout
# for x in range(10):
#     print(device.analogReadPin(0))
#     time.sleep(0.5)

# digital read-write by pin
# device.setDigitalPinMode(13, PyLIFA.pinMode["Output"])
# for x in range(10):
#     device.digitalWritePin(13, 0)
#     print(device.digitalReadPin(13))
#     time.sleep(0.5)
#     device.digitalWritePin(13, 1)
#     print(device.digitalReadPin(13))
#     time.sleep(0.5)
