import time
import serial

class PyLIFA:
    command = {
        "setArduinoMode":0,
        "flushSerial":1,
        "setPinMode":2,
        "digitalWritePin":3,
        "digitalWritePort":4,
        "tone":5,
        "digitalReadPin":6,
        "digitalReadPort":7,
        "analogReadPin":8,
        "analogReadPort":9,
        "pwmWritePin":10,
        "pwmWritePort":11,
        "sevenSegment_Configure":12,
        "sevenSegment_Write":13,
        "I2C Init":14,
        "I2C Write":15,
        "I2C Read":16,
        "SPI Init":17,
        "SPI Set Bit Order":18,
        "SPI Set Clock Divider":19,
        "SPI Set Data Mode":20,
        "SPI Send / Receive":21,
        "SPI Close":22,
        "Servo Set Num Servos":23,
        "Servo Config Servo":24,
        "Servo Write Angle":25,
        "Servo Read Angle":26,
        "Servo Write Pulse Width":27,
        "Servo Read Pulse Width":28,
        "Servo Detach":29,
        "LCD Init":30,
        "LCD Set Size":31,
        "LCD Set Cursor Mode":32,
        "LCD Clear":33,
        "LCD Set Cursor Position":34,
        "LCD Print":35,
        "LCD Sidplay Power":36,
        "LCD Scroll Display":37,
        "LCD Autoscroll":38,
        "LCD Print Direction":39,
        "LCD Create Custom Char":40,
        "LCD Print Custom Char":41,
        "contAcqOn":42,
        "contAcqOff":43,
        "getFirmwareVersion":44,
        "FiniteSampleStart":45
    }

    def __init__(self, port):
        self.port = serial.Serial(port, 115200, timeout=15)
        self.packetLen = 15
        time.sleep(2)  # wait for bootlader to finish
    def __del__(self):
        self.port.close()

    # generate packet with checksum
    def packetize(self, data):
        packet = [0]*self.packetLen
        packet[0] = 0xFF
        i = 1
        sum = 0xFF
        for d in data:
            packet[i] = d
            sum += d
            i += 1
            if i >= self.packetLen-1:
                break
        packet[self.packetLen-1] = sum % 256
        return packet

    # exchange data
    def sendReceive(self, data, bytesToRead=0, timeOut=100, maxRetries=10):
        packet = self.packetize(data)
        received = []
        for i in range(maxRetries):
            self.port.write(packet)
            received = self.port.read(bytesToRead)
            if len(received) >= bytesToRead:
                break
            else:
                time.sleep(timeOut/1000.0)
        return received

    # sync - test connection
    def sync(self):
        response = self.sendReceive([], 4)
        return response == b'sync'

    def analogReadPin(self, pin, conversionFactor = 0.0049):
        cmd = [self.command["analogReadPin"], pin]
        result = self.sendReceive(cmd, 2)
        voltage = (result[0] * 0xFF + result[1]) * conversionFactor
        return voltage

    def analogReadPort(self):
        raise Exception("Not implemented: analogReadPort()")

    def continousAcquisitionOff(self):
        self.sendReceive([self.command["contAcqOff"]])

    def continousAcquisitionSample(self, sampleRate, samplesToRead, conversionFactorSlow = 0.0049, conversionFactorFast = 0.0196):
        if sampleRate <= 1000:
            bytesToRead = 2
            conversionFactor = conversionFactorSlow
        else:
            bytesToRead = 1
            conversionFactor = conversionFactorFast
        samples = []
        for i in range(samplesToRead):
            result = self.port.read(bytesToRead)
            sample = result[0]
            if bytesToRead == 2:
                sample += result[1] * 256
            sample *= conversionFactor
            samples.append(sample)
        return samples

    def continousAcquisitionOn(self, pin, sampleRate):
        if sampleRate > 5000:
            raise Exception("Sample rate is set faster than maximum 5000 Hz")
        sampleRateInternal = sampleRate
        if sampleRate >= 2000:
            sampleRateInternal = ((sampleRate**2 * 3e-7) - (sampleRate * 0.0007) + 1.84) * sampleRate
        sampleRateInternal = int(sampleRateInternal)
        cmd = [self.command["contAcqOn"], pin, int(sampleRateInternal % 256), int(sampleRateInternal / 256)]
        self.sendReceive(cmd)

    def digitalReadPin(self, pin):
        cmd = [self.command["digitalReadPin"], pin]
        return int(self.sendReceive(cmd, 1))

    def digitalReadPort(self):
        result = self.sendReceive([self.command["digitalReadPort"]], 2)
        data = result[0] + result[1] * 256
        return int(data)

    def digitalWritePin(self, pin, value):
        cmd = [self.command["digitalWritePin"], pin, value]
        self.sendReceive(cmd, 1)

    def digitalWritePort(self, value):
        cmd = [self.command["digitalWritePort"], int(value / 256), int(value % 256)]
        self.sendReceive(cmd, 1)

    def finiteSampleStart(self, pin, sampleRate, samples):
        if sampleRate > 5000:
            raise Exception("Sample rate is set faster than maximum 5000 Hz")
        cmd = [self.command["FiniteSampleStart"], pin, int(sampleRate % 256), int(sampleRate / 256),
               int(samples % 256), int(samples / 256)]
        self.sendReceive(cmd, 1)

    def getFiniteAnalogSample(self, pin, sampleRate, samplesToRead, conversionFactorSlow = 0.0049, conversionFactorFast = 0.0196):
        self.finiteSampleStart(pin,sampleRate,samplesToRead)
        if sampleRate <= 1000:
            bytesToRead = 2
            conversionFactor = conversionFactorSlow
        else:
            bytesToRead = 1
            conversionFactor = conversionFactorFast
        samples = []
        for i in range(samplesToRead):
            result = self.port.read(bytesToRead)
            sample = result[0]
            if bytesToRead == 2:
                sample += result[1] * 256
            sample *= conversionFactor
            samples.append(sample)
        return samples

    def PWMConfigurePort(self, pins):
        for pin in pins:
            self.setDigitalPinMode(pin, self.pinMode["Output"])

    def PWMWritePin(self, pin, value):
        cmd = [self.command["pwmWritePin"], pin, value]
        self.sendReceive(cmd, 1)

    def PWMWritePort(self, pins, values):
        cmd = [self.command["pwmWritePort"]] + pins + values
        self.sendReceive(cmd, 1)

    pinMode = {
        "Input": 0,
        "Output": 1
    }

    def setDigitalPinMode(self, pin, mode):
        cmd = [self.command["setPinMode"], pin, mode]
        self.sendReceive(cmd, 1)

    # duration in ms
    def tone(self, pin, frequency, duration):
        frequency_list = []
        for i in range(2):
            frequency_list.append(frequency % 256)
            frequency = int(frequency / 256)
        frequency_list.reverse()

        duration_list = []
        for i in range(4):
            duration_list.append(duration % 256)
            duration = int(duration / 256)
        duration_list.reverse()

        cmd = [self.command["tone"], pin] + frequency_list + duration_list
        self.sendReceive(cmd, 1)

    def I2CInit(self):
        self.sendReceive([self.command["I2C Init"]], 1)

    def I2CRead(self, address, bytesToRead):
        cmd = [self.command["I2C Read"], bytesToRead, address]
        return self.sendReceive(cmd, bytesToRead)

    def I2CWrite(self, address, bytes):
        if len(bytes) > 11:
            raise Exception("Can send maximum 11 bytes of I2C data in a time")
        cmd = [self.command["I2C Write"], len(bytes), address] + bytes
        self.sendReceive(cmd, 1)

    def SPIClose(self):
        self.sendReceive([self.command["SPI Close"]], 1)

    def SPIInit(self):
        self.sendReceive([self.command["SPI Init"]], 1)

    def SPISendRecieve(self, CSPin, wordSize, bytes):
        first = True
        cmd = [self.command["SPI Send / Receive"]]
        while len(bytes):
            if first:
                first = False
                cmd += [1]  # first packet
                bytesActual = bytes[:(self.packetLen - 7)]
                del bytes[:(self.packetLen - 7)]
                cmd += [CSPin, wordSize, len(bytesActual)]
                cmd += bytesActual
            else:
                cmd += [0]  # not first packet
                bytesActual = bytes[:(self.packetLen - 5)]
                del bytes[:(self.packetLen - 5)]
                cmd += [len(bytesActual)]
                cmd += bytesActual
            self.sendReceive(cmd, len(bytesActual))

    SPIBitOrder = {
        "LSB First":0,
        "MSB First":1
    }

    def SPISetBitOrder(self, order):
        self.sendReceive([self.command["SPI Set Bit Order"], order], 1)

    SPIClockDivider = {
        2:0,
        4:1,
        8:2,
        16:3,
        32:4,
        64:5,
        128:6
    }

    def SPISetClockDivider(self, div):
        self.sendReceive([self.command["SPI Set Clock Divider"], div], 1)

    SPIDataMode = {
        "Mode 0":0,
        "Mode 1":1,
        "Mode 2":2,
        "Mode 3":3
    }

    def SPISetDataMode(self, mode):
        self.sendReceive([self.command["SPI Set Data Mode"], mode], 1)

    def configureServo(self, servoNumber, pin):
        cmd = [self.command["Servo Config Servo"], servoNumber, pin]
        self.sendReceive(cmd, 1)

    def detachServo(self, servoNumber):
        cmd = [self.command["Servo Detach"], servoNumber]
        self.sendReceive(cmd, 1)

    def servoReadAngle(self, servoNumber):
        cmd = [self.command["Servo Read Angle"], servoNumber]
        result = self.sendReceive(cmd, 1)
        return int(result[0])

    def servoReadPulseWidth(self, servoNumber):
        cmd = [self.command["Servo Read Pulse Width"], servoNumber]
        result = self.sendReceive(cmd, 2)
        return int(result[0]*256 + result[1])

    def servoWriteAngle(self, servoNumber, angle):
        cmd = [self.command["Servo Write Angle"], servoNumber, angle]
        self.sendReceive(cmd, 1)

    def servoWritePulseWidth(self, servoNumber, pulse):
        cmd = [self.command["Servo Write Pulse Width"], servoNumber, int(pulse % 256), int(pulse / 256)]
        self.sendReceive(cmd, 1)

    def setNumberOfServos(self, number):
        cmd = [self.command["Servo Set Num Servos"], number]
        result = self.sendReceive(cmd, 1)
        return int(result[0]) == 0
